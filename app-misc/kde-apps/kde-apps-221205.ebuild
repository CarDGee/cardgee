# Copyright 1999-2022 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# CarDGee Overlay

EAPI=6

DESCRIPTION="Meta package for my kde apps of choice"
HOMEPAGE="https://github.com/CarDGee/cardgee"

LICENSE="metapackage"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

RDEPEND="
	app-editors/ghostwriter
	app-office/kalendar
	app-office/kmymoney
	kde-apps/ark
	kde-apps/dolphin
	kde-apps/kmix
	kde-apps/konsole
	kde-apps/kwalletmanager
	kde-apps/kwave
	kde-apps/kwrite
	kde-apps/minuet
	kde-apps/okular
	kde-misc/bismuth
	kde-misc/kdeconnect
	kde-misc/skanlite
	media-sound/hydrogen
	net-irc/konversation
	net-p2p/ktorrent
"

pkg_postinst() {
	elog
	elog "This is a cardgee overlay ebuild"
	elog "A collection of ebuilds for my own personal use not available on portage tree."
	elog "There is no support whatsoever, but if you find a bug, please file an issue."
	elog
}

