# Copyright 1999-2022 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# CarDGee Overlay

EAPI=6

DESCRIPTION="Meta package with my selection of applications for desktop"
HOMEPAGE="https://github.com/CarDGee/cardgee"

LICENSE="metapackage"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

RDEPEND="
	app-admin/system-config-printer
	app-emulation/virt-manager
	app-office/abiword
	app-office/gnumeric
	app-shells/ohmyzsh
	app-text/atril
	app-arch/engrampa
	games-misc/c-lolcat
	lxde-base/lxappearance
	mail-client/thunderbird-bin
	mate-extra/caja-extensions
	mate-extra/mate-polkit
	media-libs/libpulse
	media-video/mpv
	net-im/signal-desktop-bin
	net-misc/ytfzf
	net-p2p/deluge
	net-print/hplip
	sys-block/gparted
"

pkg_postinst() {
	elog
	elog "This is a cardgee overlay ebuild"
	elog "A collection of ebuilds for my own personal use not available on portage tree."
	elog "There is no support whatsoever, but if you find a bug, please file an issue."
	elog
	elog "You have installed a personal desktop meta package"
	elog "It may include applications which you don't need or like"
	elog
}

