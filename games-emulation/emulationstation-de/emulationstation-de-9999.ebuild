# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

#CarDGee Overlay

EAPI=7

inherit eutils cmake git-r3

DESCRIPTION="EmulationStation Desktop Edition (ES-DE) is a frontend for browsing and launching games from your multi-platform game collection"
HOMEPAGE="https://github.com/RetroPie/EmulationStation"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

EGIT_REPO_URI="https://gitlab.com/es-de/emulationstation-de.git"
SRC_URI=""
KEYWORDS=""

COMMON_DEPEND="
	dev-cpp/eigen:3
	dev-libs/boost
	media-libs/freeimage[png,jpeg]
	media-libs/freetype
	media-libs/libsdl2
	net-misc/curl
	dev-libs/pugixml
	dev-libs/rapidjson
"
RDEPEND="${COMMON_DEPEND}"
DEPEND="${COMMON_DEPEND}"


src_configure() {
	local mycmakeargs=(
		-DCMAKE_INSTALL_PREFIX="${EPREFIX}"/usr
	)
	cmake_src_configure
}


pkg_preinst() {
	if ! has_version "=${CATEGORY}/${PN}-${PVR}"; then
		first_install="1"
	fi
}


pkg_postinst() {
	elog
	elog "This is a cardgee overlay ebuild"
	elog "A collection of ebuilds for my own personal use not available on portage tree."
	elog "There is no support whatsoever, but if you find a bug, please file an issue."
	elog
}
