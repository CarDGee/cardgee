# CarDGee overlay

CarDGee gentoo overlay
A collection of ebuilds for my own personal use not available on portage tree.
There is no support whatsoever, but if you find a bug, please file an issue.

Installation

1. Copy the cardgee.conf file to /etc/portage/repos.conf/

2. type "emaint sync -r cardgee"
